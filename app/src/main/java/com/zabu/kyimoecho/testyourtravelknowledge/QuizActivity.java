package com.zabu.kyimoecho.testyourtravelknowledge;

import android.database.SQLException;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;

import java.io.IOException;
import java.util.List;

public class QuizActivity extends FragmentActivity implements ActivityInterface  {

    public DataBaseHelper myDbHelper = new DataBaseHelper(this);

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);

        FragmentManager fm = getSupportFragmentManager();
        Fragment fragment = fm.findFragmentById(R.id.main_activity);

        if (fragment == null) {
            fragment = new LaunchFragment();
            fm.beginTransaction()
                    .add(R.id.main_activity, fragment, getString(R.string.launch_fragment_tag))
                    .commit();
        }
    }

    // Create the database if it is not already created on the device.
    // Otherwise, opens the database
    private void openDB() {

        try {
            myDbHelper.createDataBase();
        } catch (IOException ioe) {
            throw new Error("Unable to create database");
        }

        try {
            myDbHelper.openDataBase();
        }catch(SQLException sqle){
            throw sqle;
        }
    }

    @Override
    public void onReadyClicked()
    {

        // Start async thread
        new DatabaseAsyncTask().execute(this);

        List<Fragment> fragmentList = getSupportFragmentManager().getFragments();

        if (fragmentList == null) {
            return;
        }

        for (Fragment frag : fragmentList )
        {
            if (frag.getTag().equals(getString(R.string.launch_fragment_tag))) {
                Fragment quizFragment = new QuizFragment();
                switchContent(frag, quizFragment, getString(R.string.quiz_fragment_tag));
                break;
            }
        }



    }

    public void switchContent(Fragment targetFragment, Fragment sourceFragment, String tag) {
        FragmentManager fm = getSupportFragmentManager();
        fm.beginTransaction()
                .remove(targetFragment).commit();
        fm.beginTransaction()
                .add(R.id.main_activity, sourceFragment, tag)
                .commit();
    }


}
