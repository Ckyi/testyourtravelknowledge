package com.zabu.kyimoecho.testyourtravelknowledge;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by kyimoecho on 2017-02-21.
 */
public class QuizFragment extends Fragment {

    RadioGroup radioGroup;
    ImageView imageContainer;
    Button prevButton;
    Button nextButton;
    Button hintButton;
    Button submitButton;
    FragmentActivity mainActivity;
    Integer currentQuestionIndex;
    ArrayList<Question> questionList = null;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof FragmentActivity){
            mainActivity = (FragmentActivity) context;
        }

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_quiz, container, false);

        nextButton = (Button) v.findViewById(R.id.next);
        prevButton = (Button) v.findViewById(R.id.prev);
        hintButton = (Button) v.findViewById(R.id.hint);
        submitButton = (Button) v.findViewById(R.id.submit);

        imageContainer = (ImageView) v.findViewById(R.id.imageContainer);
        radioGroup = (RadioGroup) v.findViewById(R.id.radioOptionGroup);

        prevButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                questionList.get(currentQuestionIndex)
                        .set_user_selection(radioGroup.getCheckedRadioButtonId());

                currentQuestionIndex--;

                if (currentQuestionIndex < 0) {
                    currentQuestionIndex = questionList.size() - 1;
                }

                updateView(currentQuestionIndex);
            }

        });

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                questionList.get(currentQuestionIndex)
                        .set_user_selection(radioGroup.getCheckedRadioButtonId());

                currentQuestionIndex++;
                currentQuestionIndex %= questionList.size();

                updateView(currentQuestionIndex);

            }

        });

        hintButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast toast = Toast.makeText(mainActivity, questionList.get(currentQuestionIndex).get_hint(),
                        Toast.LENGTH_LONG);
                toast.show();
            }

        });

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Boolean complete = true;

                for(Question q: questionList) {
                    if(q.get_user_selection() == -1) {
                        complete = false;
                        break;
                    }
                }

                if(complete) {
                    showDialog(getString(R.string.submit_satisfy),
                            getString(R.string.submit_proceed),getString(R.string.submic_cancel));
                } else {
                    showDialog(getString(R.string.submit_questions_left),
                            getString(R.string.submit_proceed),getString(R.string.submic_cancel));
                }

            }

            // Show warning dialogbox
            private void showDialog(String msg, String posLabel, String negLabel)
            {
                AlertDialog.Builder builder1 = new AlertDialog.Builder(mainActivity);
                builder1.setMessage(msg);
                builder1.setCancelable(true);

                builder1.setPositiveButton(
                        posLabel,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                builder1.setNegativeButton(
                        negLabel,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert11 = builder1.create();
                alert11.show();

            }

        });

        if (savedInstanceState != null) {
            currentQuestionIndex = savedInstanceState.getInt("currentQuestionIndex");
            updateView(currentQuestionIndex);

        }

        return v;
    }

    public ArrayList<Question> getQuestionList() {
        return questionList;
    }

    public void setQuestionList(ArrayList<Question> questionList) {
        this.questionList = questionList;
    }


    // Initialize view with the first question
    public void initalizeView(final ArrayList<Question> questionList)
    {
        this.questionList = questionList;

        /*Question question = questionList.get(0);

        imageContainer.setImageBitmap(question.get_image());

        String[] confusion = question.get_confusion();

        // Initialize radio buttons
        for(int i=0; i < radioGroup.getChildCount(); i++) {
            ((RadioButton) radioGroup.getChildAt(i)).setText(confusion[i]);
        }

        question.set_user_selection(radioGroup.getCheckedRadioButtonId()); */

        updateView(0);


    }


    public void updateView(Integer index)
    {
        if (questionList == null || questionList.size() == 0) return;

        Question question = questionList.get(index);

        imageContainer.setImageBitmap(question.get_image());

        String[] confusion = question.get_confusion();

        // Initialize radio buttons
        for(int i=0; i < radioGroup.getChildCount(); i++) {
            ((RadioButton) radioGroup.getChildAt(i)).setText(confusion[i]);
        }

        currentQuestionIndex = index;
    }


    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);

        savedInstanceState.putInt("currentQuestionIndex", currentQuestionIndex);
    }
}
