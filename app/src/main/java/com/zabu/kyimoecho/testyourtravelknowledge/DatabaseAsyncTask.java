package com.zabu.kyimoecho.testyourtravelknowledge;

import android.database.Cursor;
import android.os.AsyncTask;
import java.util.ArrayList;

/**
 * Created by kyimoecho on 2017-02-26.
 */

public class DatabaseAsyncTask extends AsyncTask<QuizActivity, Void, ArrayList<Question>> {

    private QuizActivity mQuizActivity;

    @Override
    protected ArrayList<Question> doInBackground(QuizActivity... context) {

        mQuizActivity = context[0];
        ArrayList<Question> questionList = new ArrayList<>();

        Cursor res = mQuizActivity.myDbHelper.getQuestionsByContinent("Asia"); // This will be replaced by spinner value

        res.moveToFirst();

        while(res.isAfterLast() == false){
            Question question = new Question();
            question.set_name(res.getString(res.getColumnIndex(DataBaseHelper.MONUMENTS_COLUMN_NAME)));
            question.set_image(res.getBlob(res.getColumnIndex(DataBaseHelper.MONUMENTS_COLUMN_IMAGE)));
            question.set_continents(res.getString(res.getColumnIndex(DataBaseHelper.MONUMENTS_COLUMN_CONTINENTS)));
            question.set_country(res.getString(res.getColumnIndex(DataBaseHelper.MONUMENTS_COLUMN_COUNTRY)));
            question.set_region(res.getString(res.getColumnIndex(DataBaseHelper.MONUMENTS_COLUMN_REGION)));
            question.set_city_area(res.getString(res.getColumnIndex(DataBaseHelper.MONUMENTS_COLUMN_CITY_AREA)));
            question.set_confusion_1(res.getString(res.getColumnIndex(DataBaseHelper.MONUMENTS_COLUMN_CONFUSION_1)));
            question.set_confusion_2(res.getString(res.getColumnIndex(DataBaseHelper.MONUMENTS_COLUMN_CONFUSION_2)));
            question.set_confusion_3(res.getString(res.getColumnIndex(DataBaseHelper.MONUMENTS_COLUMN_CONFUSION_3)));
            question.set_confusion_4(res.getString(res.getColumnIndex(DataBaseHelper.MONUMENTS_COLUMN_CONFUSION_4)));
            question.set_hint(res.getString(res.getColumnIndex(DataBaseHelper.MONUMENTS_COLUMN_HINT)));

            questionList.add(question);

            res.moveToNext();
        }

        mQuizActivity.myDbHelper.close();

        return questionList;
    }

    @Override
    protected void onPostExecute(ArrayList<Question> result) {

    }
}