package com.zabu.kyimoecho.testyourtravelknowledge;

/**
 * Created by kyimoecho on 2017-02-21.
 */
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.sql.Blob;
import java.util.UUID;

public class Question {

    private Integer _id;
    private String _name;
    private Bitmap _image;
    private String _continents;
    private String _region;
    private String _country;
    private String _city_area;
    private String _confusion_1;
    private String _confusion_2;
    private String _confusion_3;
    private String _confusion_4;
    private String _hint;
    private String _description;

    public Question() { }

    public Integer get_id() {
        return _id;
    }

    public String get_name() {
        return _name;
    }

    public void set_name(String _name) {
        this._name = _name;
    }


    public byte[] get_image() {
        return _image;
    }

    // Convert byte array into bitmap image and stored
    public void set_image(byte[] _image) {
        Bitmap bm = BitmapFactory.decodeByteArray(_image, 0, _image.length);
        this._image = bm;
    }

    public String get_continents() {
        return _continents;
    }

    public void set_continents(String _continents) {
        this._continents = _continents;
    }

    public String get_region() {
        return _region;
    }

    public void set_region(String _region) {
        this._region = _region;
    }

    public String get_country() {
        return _country;
    }

    public void set_country(String _country) {
        this._country = _country;
    }

    public String get_city_area() {
        return _city_area;
    }

    public void set_city_area(String _city_area) {
        this._city_area = _city_area;
    }

    public String get_confusion_1() {
        return _confusion_1;
    }

    public void set_confusion_1(String _confusion_1) {
        this._confusion_1 = _confusion_1;
    }

    public String get_confusion_2() {
        return _confusion_2;
    }

    public void set_confusion_2(String _confusion_2) {
        this._confusion_2 = _confusion_2;
    }

    public String get_confusion_3() {
        return _confusion_3;
    }

    public void set_confusion_3(String _confusion_3) {
        this._confusion_3 = _confusion_3;
    }

    public String get_confusion_4() {
        return _confusion_4;
    }

    public void set_confusion_4(String _confusion_4) {
        this._confusion_4 = _confusion_4;
    }

    public String get_hint() {
        return _hint;
    }

    public void set_hint(String _hint) {
        this._hint = _hint;
    }

    public String get_description() {
        return _description;
    }

    public void set_description(String _description) {
        this._description = _description;
    }

}
