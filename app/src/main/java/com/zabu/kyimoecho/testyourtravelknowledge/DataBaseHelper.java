package com.zabu.kyimoecho.testyourtravelknowledge;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;



/**
 *  The core framework of this class is implemented by Juan-Manuel Fluxà.
 *  http://blog.reigndesign.com/blog/using-your-own-sqlite-database-in-android-applications/
 */
public class DataBaseHelper extends SQLiteOpenHelper {

    //The Android's default system path of your application database.
    private static String DB_PATH = "/data/data/com.zabu.kyimoecho.testyourtravelknowledge/databases/";
    private static String DB_NAME = "quiz.db";
    private static final String TABLE_MONUMENTS = "Monuments";

    // Columns
    public static final String MONUMENTS_COLUMN_NAME = "CONTINENTS";
    public static final String MONUMENTS_COLUMN_IMAGE = "IMAGE";
    public static final String MONUMENTS_COLUMN_CONTINENTS = "CONTINENTS";
    public static final String MONUMENTS_COLUMN_REGION = "REGION";
    public static final String MONUMENTS_COLUMN_COUNTRY = "COUNTRY";
    public static final String MONUMENTS_COLUMN_CITY_AREA = "CITY_AREA";
    public static final String MONUMENTS_COLUMN_CONFUSION_1 = "CONFUSION_1";
    public static final String MONUMENTS_COLUMN_CONFUSION_2 = "CONFUSION_2";
    public static final String MONUMENTS_COLUMN_CONFUSION_3 = "CONFUSION_3";
    public static final String MONUMENTS_COLUMN_CONFUSION_4 = "CONFUSION_4";
    public static final String MONUMENTS_COLUMN_HINT = "HINT";


    private SQLiteDatabase myDataBase;
    private final Context myContext;

    /**
     * Constructor
     * Takes and keeps a reference of the passed context in order to access to the application assets and resources.
     *
     * @param context
     */
    public DataBaseHelper(Context context) {

        super(context, DB_NAME, null, 1);
        this.myContext = context;
    }

    /**
     * Creates a empty database on the system and rewrites it with your own database.
     */
    public void createDataBase() throws IOException {

        boolean dbExist = checkDataBase();

        if (dbExist) {
            //do nothing - database already exist
        } else {

            //By calling this method and empty database will be created into the default system path
            //of your application so we are gonna be able to overwrite that database with our database.
            this.getReadableDatabase();

            try {

                copyDataBase();

            } catch (IOException e) {

                throw new Error("Error copying database");

            }
        }

    }

    /**
     * Check if the database already exist to avoid re-copying the file each time you open the application.
     *
     * @return true if it exists, false if it doesn't
     */
    private boolean checkDataBase() {

        SQLiteDatabase checkDB = null;

        try {
            String myPath = DB_PATH + DB_NAME;
            checkDB = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);

        } catch (SQLiteException e) {

            //database does't exist yet.

        }

        if (checkDB != null) {

            checkDB.close();

        }

        return checkDB != null ? true : false;
    }

    /**
     * Copies your database from your local assets-folder to the just created empty database in the
     * system folder, from where it can be accessed and handled.
     * This is done by transfering bytestream.
     */
    private void copyDataBase() throws IOException {

        //Open your local db as the input stream
        InputStream myInput = myContext.getAssets().open(DB_NAME);

        // Path to the just created empty db
        String outFileName = DB_PATH + DB_NAME;

        //Open the empty db as the output stream
        OutputStream myOutput = new FileOutputStream(outFileName);

        //transfer bytes from the inputfile to the outputfile
        byte[] buffer = new byte[1024];
        int length;
        while ((length = myInput.read(buffer)) > 0) {
            myOutput.write(buffer, 0, length);
        }

        //Close the streams
        myOutput.flush();
        myOutput.close();
        myInput.close();

    }

    public void openDataBase() throws SQLException {

        //Open the database
        String myPath = DB_PATH + DB_NAME;
        myDataBase = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);

    }

    @Override
    public synchronized void close() {

        if (myDataBase != null)
            myDataBase.close();

        super.close();

    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public Cursor getQuestionsByContinent(String continent) {

        String condition = MONUMENTS_COLUMN_CONTINENTS + "='" + continent + "'";
        Cursor res =  myDataBase.query(TABLE_MONUMENTS, null, condition,null,null,null,null);

        return res;
    }
}