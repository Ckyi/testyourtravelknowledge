package com.zabu.kyimoecho.testyourtravelknowledge;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Spinner;


/**
 * Created by kyimoecho on 2017-02-23.
 */
public class LaunchFragment extends Fragment {

    Button readyButton;
    Spinner continentSpinner;
    FragmentActivity mainActivity;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof FragmentActivity){
            mainActivity = (FragmentActivity) context;
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_launch, container, false);

        readyButton = (Button) v.findViewById(R.id.ready); // By using view returned by the inflater
                                                           // you will only have access to those elements
                                                           // within that fragment
        readyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    ((ActivityInterface) mainActivity).onReadyClicked();
                }catch (ClassCastException cce){

                }
            }

        });

        //continentSpinner = (Spinner) v.findViewById(R.id.continent);


        return v;
    }


}

